# 小鹤双拼学习

## 官方介绍

![小鹤双拼键位表](https://flypy.com/images/hejp.png)

双拼方案：主要是韵母键位的设计方案，单声母键位不变

即把三个双声母 zh ch sh 和35个韵母安排到键盘的26个字母键位上的设计方案

零声母方案：指全拼中无声母韵母音节在双拼中的处理方案

本方案以韵母首字母为零声母，即把韵母的首字母当作声母

  单字母韵母，零声母 + 韵母所在键，如： 啊＝aa  哦=oo  额=ee

  双字母韵母，零声母 + 韵母末字母（表现形式同全拼），如： 爱＝ai  恩=en  欧=ou

  三字母韵母，零声母 + 韵母所在键，如： 昂＝ah

---

记忆口诀①（官方版）：
![记忆口诀①（官方版）](https://upload-images.jianshu.io/upload_images/4509641-5a2bfb4cce3061cb.png)

--- 

<table width="95%" border="0" cellspacing="0" cellpadding="0">
      <tbody><tr>
        <td width="6%" height="20"><span class="text-hw">Kuai</span></td>
        <td width="6%"><span class="text-hw">_ing</span></td>
        <td width="6%"><span class="text-hw">Liang</span></td>
        <td width="6%"><span class="text-hw">_uang</span></td>
        <td width="6%"><span class="text-hw">Ruan</span></td>
        <td width="6%"><span class="text-hw">Cao</span></td>
        <td width="6%"><span class="text-hw">Zou</span></td>
        <td width="6%"></td>
        <td width="6%"><span class="text-hw">T</span></td>
        <td width="6%"><span class="text-hw">_ue_ve</span></td>
        <td width="6%"><span class="text-hw">Qiu</span></td>
        <td width="6%"><span class="text-hw">Yun</span></td>
        <td width="6%"><span class="text-hw">Wei</span></td>
        <td width="6%"><span class="text-hw">J_an</span></td>
        <td width="6%"><span class="text-hw">Mian</span></td>
      </tr>
      <tr>
        <td height="20"><span class="text-hw">快</span></td>
        <td><span class="text-hw">迎</span></td>
        <td><span class="text-hw">两</span></td>
        <td><span class="text-hw">王</span></td>
        <td><span class="text-hw">软</span></td>
        <td><span class="text-hw">草</span></td>
        <td><span class="text-hw">走</span></td>
        <td><span class="text-hw"> </span></td>
        <td><span class="text-hw">特</span></td>
        <td><span class="text-hw">约</span></td>
        <td><span class="text-hw">秋</span></td>
        <td><span class="text-hw">云</span></td>
        <td><span class="text-hw">为</span></td>
        <td><span class="text-hw">见</span></td>
        <td><span class="text-hw">面</span></td>
      </tr>
      <tr>
        <td height="10">&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td height="20"><span class="text-hw">Xia</span></td>
        <td><span class="text-hw">_ua</span></td>
        <td><span class="text-hw">Song</span></td>
        <td><span class="text-hw">_iong</span></td>
        <td><span class="text-hw">U_shu</span></td>
        <td><span class="text-hw">I_chi</span></td>
        <td><span class="text-hw">V_zhui_v</span></td>
        <td><span class="text-hw"></span></td>
        <td><span class="text-hw">Geng</span></td>
        <td><span class="text-hw">Dai</span></td>
        <td><span class="text-hw">Bin</span></td>
        <td><span class="text-hw">Niao</span></td>
        <td><span class="text-hw">Fen</span></td>
        <td><span class="text-hw">Pie</span></td>
        <td><span class="text-hw">Hang</span></td>
      </tr>
      <tr>
        <td height="20"><span class="text-hw">夏</span></td>
        <td><span class="text-hw">娃</span></td>
        <td><span class="text-hw">怂</span></td>
        <td><span class="text-hw">恿</span></td>
        <td><span class="text-hw">书</span></td>
        <td><span class="text-hw">痴</span></td>
        <td><span class="text-hw">追</span></td>
        <td><span class="text-hw"></span></td>
        <td><span class="text-hw">更</span></td>
        <td><span class="text-hw">待</span></td>
        <td><span class="text-hw">滨</span></td>
        <td><span class="text-hw">鸟</span></td>
        <td><span class="text-hw">分</span></td>
        <td><span class="text-hw">撇</span></td>
        <td><span class="text-hw">航</span></td>
      </tr>
    </tbody></table>

## 训练方法
> 注意：尽量盲打，少看键盘

- 对照键位表，先打一篇40~100字的文章或段落，熟悉一下感觉
- 声母很简单，只要记住zh、sh、ch的键位即可
- 中间 `F` `G` `H` `J` 对应着 `en` `eng` `ang` `an`，再定神一看，`ong(S)` `ing(K)` 也在同一排
- 再看第三排那八个键位，其中 `N` `M` 相信你如果照我说的实际操作过了，`iao` `ian` 的使用频率一定会令你印象颇深，`C` `D` 键 一个 `ao` 一个 `ai` 很好记
- 第二、三排里 `ou(Z)` `in(B)`也都是常用键，其余的可以直接记下来
- 只剩第一排，`ei(W)` `ie(P)`也算常用，再除去三个只表示声母的键子，还剩五个，可以直接记下来

## 个人感受
经过试验官方口诀，和各种速记口诀，发现真的没什么卵用，只有硬背对照表才是王道

<style>
table {
    white-space: normal;
    line-height: normal;
    font-weight: normal;
    font-size: medium;
    font-style: normal;
    color: -internal-quirk-inherit;
    text-align: start;
    font-variant: normal;
    -webkit-border-horizontal-spacing: 0px;
    -webkit-border-vertical-spacing: 0px;
    width: 100%;
    border-top-width: 0px;
    border-right-width: 0px;
    border-bottom-width: 0px;
    border-left-width: 0px;
}
</style>