# vscode七牛图片上传插件改造

> 将 [Visual Studio Code](https://code.visualstudio.com/) 打造为最好用的开发人员文档书写工具 <i class="fa fa-wrench"></i>

## 插件安装

### qiniu-upload-image

一个 VS Code 插件，编写 Markdown 时可以快捷上传本地图片获取七牛图床外链。

### Features

![priview](https://raw.githubusercontent.com/yscoder/vscode-qiniu-upload-image/master/features/demo.gif)

> Tips: 只有在编辑 Markdown 时插件才可使用。

### Usage

1. 粘贴图片路径上传：`SHIFT + P`
2. 直接选择图片上传：`SHIFT + O`

> 按键需在英文编辑状态下有效，功能2 需要升级 vscode 到 v1.17+。

### Install

`Ctrl+P` 输入命令：

```bash
ext install qiniu-upload-image
```

### User Settings

```js
{
    // 插件开关
    "qiniu.enable": true,

    // 一个有效的七牛 AccessKey 签名授权
    "qiniu.access_key": "*****************************************",

    // 一个有效的七牛 SecretKey 签名授权
    "qiniu.secret_key": "*****************************************",

    // 七牛图片上传空间
    "qiniu.bucket": "ysblog",

    // 七牛图片上传路径，参数化命名，暂时支持 ${fileName}、${mdFileName}、${date}、${dateTime}
    // 示例：
    //   ${fileName}-${date} -> picName-20160725.jpg
    //   ${mdFileName}-${dateTime} -> markdownName-20170412222810.jpg
    "qiniu.remotePath": "${fileName}",

    // 七牛图床域名
    "qiniu.domain": "http://xxxxx.xxxx.com"
}
```

### Repository

[https://github.com/yscoder/vscode-qiniu-upload-image](https://github.com/yscoder/vscode-qiniu-upload-image)

**Enjoy!**



## 插件改造

### 使用不满足需求的地方
- 文件名有重复的概率，需要加强
- 七牛图片返回结果需要加图片样式

### 改造步骤

#### 重新安装

从扩展应用商店安装的貌似是1.0.0版本，但作者 [<i class="fa fa-github"></i> Github](https://github.com/yscoder/vscode-qiniu-upload-image) 上貌似是 `1.1.0` 版本。

找到 `qiniu-upload-image-1.1.0.vsix` 下载安装。已从应用商店安装的，先卸载，然后重新启动vscode，必须重启，否则容易出问题。

#### 改造代码

找到插件的安装地址 `%USERPROFILE%\.vscode\extensions\imys.qiniu-upload-image-1.1.0`

用vscode打开文件夹，开始改造！

##### 增加图片样式参数

打开 `package.json` 文件，找到 `"qiniu.domain"` 节点，再下面再补上一个，如下代码

```json
    "qiniu.domain": {
        "type": "string",
        "default": "",
        "description": "七牛图床域名。"
    },
    "qiniu.img_style": {
        "type": "string",
        "default": "",
        "description": "七牛图片样式。"
    }
```

##### 将图片样式参数加入返回链接中

打开 `extension.js` 文件，第15行，改造为如下代码
```javascript
const img = `![${name}](${url}${config.img_style})`;
```

##### 文件名增加一个随机数字的参数 ${randNum}

打开 `lib/upload.js` 文件，在第12行找到 `formatParam` 方法，改造为如下代码
```javascript
// 默认参数
const formatParam = (file, mdFileName) => {
    const dt = new Date()
    const y = dt.getFullYear()
    const m = dt.getMonth() + 1
    const d = dt.getDate()
    const h = dt.getHours()
    const mm = dt.getMinutes()
    const s = dt.getSeconds()
    const r = Math.floor(1000+Math.random()*10)

    const date = `${y}${m}${d}`
    var ext = path.extname(file)

    return {
        date,
        dateTime: `${date}${h}${mm}${s}`,
        fileName: path.win32.basename(file, ext),
        randNum: `${r}`,
        ext,
        mdFileName
    }
}
```