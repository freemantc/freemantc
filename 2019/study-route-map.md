# 2019年学习路线图

> <i class="fa fa-map-signs"></i> 新的一年，要有新的突破！

## 目标

针对企业信息管理系统进行快速的开发

## 原则

- 跨平台
- 前后端分离
- 提升开发效率
    - 代码生成
    - 项目模板
    - 自动化构建

## 技术路线图

### 数据库
- postgresql

### 持久化
- dapper
- T4 模板生成

### 后端技术
- .net core web API
- IoC 依赖注入
- AOP 基于切面编程技术实现全局日志和异常的统一处理
- Async和Await 异步编程
- Swagger 接口文档
- Cros 跨域解决方案
- JWT 权限验证
- IdentityServer4 的使用
- 创建自己的.net模板

### 缓存
- redis 分布式缓存

### 前端技术
- Vue 全家桶（Vue + VueRouter + Axios + vue-cli + vuex + Webpack）
- ElementUI 基于Vue的组件库
- D2Admin Vue+ElementUI 的企业中后台产品前端集成方案


## 参考资料

> - [从壹开始前后端分离（系列文章）][1]
> - [dotnet core +vue+elementUI 的一个例子][2]
> - [针对 ASP.NET Core Web API 的先进架构][3]
> - [在 ASP.NET Core 依赖注入][4]
> - [ASP.NET Core 中的日志记录][5]
> - [dotnet new 自定义模板][6]
> - [ASP.NET Core 中文文档][7]
> - [IdentityServer4 中文文档与实战][8]

[1]:https://www.cnblogs.com/laozhang-is-phi/category/1305247.html "从壹开始前后端分离"
[2]:https://github.com/hbb0b0/Hbb0b0.CMS "dotnet core +vue+elementUI 的一个例子"
[3]:https://www.infoq.cn/article/advanced-architecture-aspnet-core "针对 ASP.NET Core Web API 的先进架构"
[4]:https://docs.microsoft.com/zh-cn/aspnet/core/fundamentals/dependency-injection?view=aspnetcore-2.2 "在 ASP.NET Core 依赖注入"
[5]:https://docs.microsoft.com/zh-cn/aspnet/core/fundamentals/logging/index?view=aspnetcore-2.2 "ASP.NET Core 中的日志记录"
[6]:https://docs.microsoft.com/zh-cn/dotnet/core/tools/custom-templates "dotnet new 自定义模板"
[7]:http://www.cnblogs.com/dotNETCoreSG/p/aspnetcore-index.html "ASP.NET Core 中文文档目录"
[8]:http://www.cnblogs.com/stulzq/p/8119928.html "IdentityServer4 中文文档与实战"