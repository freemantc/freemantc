<!-- + [首页](/) -->

+ [<i class="fa fa-code-fork"></i> 作者简历 :100:](/resume-taocheng.md)

+ 2021年文档
    + [Linux安装chrome](/2021/install-chrome-on-linux.md)
    + [免费编辑PDF文件](/2021/edit-pdf-free.md)

+ 2020年文档
    + [Markdown 入门](/2020/markdown-getting-started.md)
    + [Feign 入门指引](/2020/feign-getting-started.md)

+ 2019年文档
    + [win10增加图片查看器](/2019/win10-photoviewer.md)
    + [小鹤双拼学习](/2019/flypy-study.md)
    + [Macaron马卡龙系颜色表](/2019/macaron-colors.md)
    + [vscode七牛图片上传插件改造](/2019/vscode-qiniu-upload-imaage.md)
    + [2019年学习路线图](/2019/study-route-map.md)

+ 2018年文档
    + [Markdown Reader 插件改造](/2018/markdown-reader-extensions-modify.md)
    + [使用vscode+Pandoc导出PDF文档](/2018/vscode-pandoc-to-pdf.md)
    + [使用Yummy-Jekyll构建自己的博客](/2018/use-yummy-jekyll-build-blogs.md)
    + [PostgreSQL 获取表结构](/2018/postgresql-table-info.md)

+ 系列文章
    + [面向对象编程](/object-oriented-programming/)
        - [基础知识](/object-oriented-programming/basic.md)
        - [类与接口](/object-oriented-programming/class-interface.md)
        - [多态详解](/object-oriented-programming/polymorphism.md)
        - [工厂模式](/object-oriented-programming/factory.md)
        - [单例模式](/object-oriented-programming/singleton.md)
        - [依赖注入与控制反转](/object-oriented-programming/di-ioc.md)
    + [Quiver快速入门](/quiver-tutorial/)
        - [开始使用](/quiver-tutorial/01-getting-started.md)
        - [单元格类型](/quiver-tutorial/02-cell-types.md)
        - [单元格操作](/quiver-tutorial/03-cell-operations.md)
        - [图像、文件和链接](/quiver-tutorial/04-images-files-and-links.md)
        - [预览和演示模式](/quiver-tutorial/05-preview-and-presentation.md)
        - [全文搜索](/quiver-tutorial/06-full-text-search.md)
        - [标签](/quiver-tutorial/07-tagging.md)
        - [云同步](/quiver-tutorial/08-cloud-syncing.md)
        - [团队协作](/quiver-tutorial/09-team-collaboration.md)
        - [备份与恢复](/quiver-tutorial/10-backup-and-recovery.md)
        - [导入与导出](/quiver-tutorial/11-import-and-export.md)
        - [首选项](/quiver-tutorial/12-preferences.md)


<div style="margin-bottom: 20px; padding: 6px;border-bottom: 1px solid #eee;"></div>

#### <i class="fa fa-link"></i> LINKS
- [BillySir的编程思维 <i class="fa fa-external-link"></i>](https://billysir.cnblogs.com/)
