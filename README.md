# IT山人的知识库

## 使用说明

1. 克隆该项目（<https://gitee.com/freemantc/freemantc.git>）到本地
2. 用 vscode 打开克隆到本地的目录
3. 编写自己的 markdown 文档，比如：`举个栗子.md`
4. 修改 `_sidebar.md` 文件，参照已有例子，把刚才的 `举个栗子.md` 添加进去
5. 安装 vscode 的插件 `Live Server` （已安装的跳过）
6. 点 vscode 右下角的 <kbd> <i class="fa fa-podcast"></i> Go Live 按钮 </kbd>
7. 访问 <http://127.0.0.1:5000/> 进行预览

## 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
