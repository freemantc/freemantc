# 使用vscode+Pandoc导出PDF文档

<!-- \newpage -->

> 将 [Visual Studio Code](https://code.visualstudio.com/) 打造为最好用的开发人员文档书写工具 <i class="fa fa fa-wrench"></i>

## 需要的工具与环境

- [Visual Studio Code（VS Code）：文档书写工具](###Visual-Studio-Code)
- [Markdown Preview Enhanced：VS Code的插件](###Markdown-Preview-Enhanced)
- [Pandoc：格式转换的「瑞士军刀」](###Pandoc)
- [LaTeX：基于ΤΕΧ的排版系统](###LaTeX)
- [PlantUML：开源的UML快速绘制工具](###PlantUML)
- [ImageMagick：开源图片处理工具](###ImageMagick)



### Visual Studio Code

微软出品的跨平台编辑器，开源免费，功能强大。目前所使用过的最好用的 markdown 书写工具。

下载地址：https://code.visualstudio.com/



### Markdown Preview Enhanced

Markdown Preview Enhanced 是一款为 Atom 以及 Visual Studio Code 编辑器编写的超级强大的 Markdown 插件。 这款插件意在让你拥有飘逸的 Markdown 写作体验。

![Markdown Preview Enhanced 介绍](https://user-images.githubusercontent.com/1908863/28227953-eb6eefa4-68a1-11e7-8769-96ea83facf3b.png)

安装方法 

打开 vscode 编辑器，在插件页搜索 markdown-preview-enhanced，接着点击 Install 按钮。

![Markdown Preview Enhanced 安装](https://user-images.githubusercontent.com/1908863/28199365-bb03a570-682a-11e7-8f65-d7d2b258d583.png)



#### Markdown Preview Enhanced 小改造
在导出 PDF 的时候，让 front-matter 中的 template 参数支持相对路径的转换，便于团队合作。

修改一：template 参数支持项目相对路径
```javascript
// 打开插件安装目录的 node_modules\@shd101wyy\mume\out\src\pandoc-convert.js 文件
// 找到 if (config["template"]) ，大约在127行附近，修改为
// 由于 processOutputConfig 方法没有 projectDirectoryPath 参数，
// 所以需要从外部传入，记得声明和调用都需要加上
if (config["template"]) {
    var templateFilePath = config["template"];
    if (templateFilePath.startsWith("/")) {
        templateFilePath = path.resolve(projectDirectoryPath, "." + templateFilePath);
    }
    args.push("--template=" + templateFilePath);
}
```

修改二：增加换页符转换
```javascript
// 打开插件安装目录的 node_modules\@shd101wyy\mume\out\src\pandoc-convert.js 文件
// 找到 pandocConvert 方法 ，大约在256行附近，在该方法的合适位置添加如下代码

// 转换新页符，此方法只对pandoc使用xelatex引擎的模板转换有效
text = text.replace(/<!-- \\newpage -->/g, "\\newpage");
```

修改三：支持 fontawesome 图标
```javascript
// 前提一：安装fontawesome5宏包
// 前提二：模板文件中增加fontawesome5宏包引用， \usepackage{fontawesome5}
// 前提三：在front-matter中增加 fontawesome: true

// 打开插件安装目录的 node_modules\@shd101wyy\mume\out\src\pandoc-convert.js 文件
// 还是 pandocConvert 方法，在该方法的合适位置添加如下代码

// 处理fontawesonme5标签
if(config["fontawesome"]){
    var reg = new RegExp('<i class="fa(\\S?) fa-(\\S+)"></i>','gm');
    text = text.replace(reg,"\\faIcon{$2}");
}
```



### Pandoc

Pandoc 是由John MacFarlane开发的标记语言转换工具，可实现不同标记语言间的格式转换，堪称该领域中的“瑞士军刀”。

官方网站：http://pandoc.org/

下载安装，并没有需要特别注意的地方。安装完成后，在命令行输入如下代码，检查安装是否成功。

```bash
    pandoc -v
```

若要使用 pandoc 导出 PDF 文档的话，就必须使用 LaTeX 。



### LaTeX

摘自维基百科

> LaTeX， 是一种基于TEX的排版系统，由美国电脑学家莱斯利·兰伯特在20世纪80年代初期开发，利用这种格式，即使用户没有排版和程序设计的知识也可以充分发挥由TEX所提供的强大功能，能在几天，甚至几小时内生成很多具有书籍质量的印刷品。对于生成复杂表格和数学公式，这一点表现得尤为突出。因此它非常适用于生成高印刷质量的科技和数学类文档。这个系统同样适用于生成从简单的信件到完整书籍的所有其他种类的文档。

Pandoc 推荐在 windows 下使用 [MikTex](https://miktex.org/)，但 TexLive 对环境的配置则更加傻瓜。不过 MikTex 现在只有基础安装版，所需要的宏包都要自行安装，而且安装源都是国外的，不是很稳定（俄罗斯的源貌似还可以），TexLive 安装完需要约 5G 的磁盘空间。

[miktex清华大学开源软件镜像站](https://mirrors.tuna.tsinghua.edu.cn/CTAN/systems/win32/miktex/setup/)

[texlive清华大学开源软件镜像站](https://mirrors.tuna.tsinghua.edu.cn/CTAN/systems/texlive/Images/)

#### MiKTeX 中文支持的解决方案

以管理员身份启动 MiKTeX Console，切换到 Packages 面板，搜索并安装下列 Packages（宏包）

- ctex
- l3kernel
- l3packages
- ms
- cjk
- cjkpunct
- miktex-cjkutils （若系统没自动安装，就给装上）
- ulem
- zhnumber
- zhmetrics



### PlantUML

轻松从简单的文字说明创建UML图。也有许多种可用的图表。它也可以在PNG, LaTeX, EPS, SVG图像导出。

打开 vscode 编辑器，在插件页搜索 PlantUML，接着点击 Install 按钮。

#### PlantUML插件依赖环境

- 安装 `Java` 环境
- 安装 `GraphViz` 程序 [下载](http://www.graphviz.org/download/)
  - 将 `GraphViz` 可执行程序路径（bin目录）配置到 `$path`
  - 添加环境变量 `$GRAPHVIZ_DOT`（bin目录下的dot.exe程序）
- 安装 `PlantUML` 插件（alt+D预览图片）



### ImageMagick

ImageMagick是一个免费的创建、编辑、合成图片的软件。它可以读取、转换、写入多种格式的图片。图片切割、颜色替换、各种效果的应用，图片的旋转、组合，文本，直线，多边形，椭圆，曲线，附加到图片伸展旋转。ImageMagick是免费软件：全部源码开放，可以自由使用，复制，修改，发布，它遵守GPL许可协议，可以运行于大多数的操作系统，ImageMagick的大多数功能的使用都来源于命令行工具。

[ImageMagick 下载](http://www.imagemagick.org/script/download.php)

<i class="fa fa-apple"></i> mac系统推荐使用 Homebrew 进行安装
```bash
brew install imagemagick
```

<!-- pagebreak -->
<!-- \newpage -->

## 示例

### 一段js代码
```javascript
var uploader = WebUploader.create({
    swf: '@basePath/webuploader/Uploader.swf',
    server: '@Url.Action("Upload", "Video")',
    pick: { id: '#picker', multiple: false },
    accept: { 
        title: '视频文件', 
        extensions: 'flv,mp4', 
        mimeTypes: 'video/*' 
    },
    resize: false,
    chunked: true, //开始分片上传
    chunkSize: 2 * 1024 * 1024, //每一片的大小（2*1024*1024=2M）
    threads: 1, //上传并发数。允许同时最大上传进程数。
    fileNumLimit: 1, //验证文件总数量, 超出则不允许加入队列。
    formData: {
        guid: GUID, //自定义参数
        fileName: $("#VideoFile").val()
    }
});
```

<!-- \newpage -->

### 分片上传的 MVC3 接收方法
```csharp
[HttpPost]
public ActionResult Upload()
{
    var rootPath = Server.MapPath(ConnString.FilePath);
    _basePath = System.IO.Path.Combine(rootPath, 
                ConnString.VideoFilePath.Trim('/'));
    _chunkDirPath = System.IO.Path.Combine(_basePath, ChunkDir);
    if (!System.IO.Directory.Exists(_chunkDirPath))
    {
        System.IO.Directory.CreateDirectory(_chunkDirPath);
    }

    var file = Request.Files[0];//表单中取得分块文件
    var chunk = Request.Form["chunk"];

    var clientFileName = Request.Form["name"];
    var fileName = Guid.NewGuid().ToString("N") 
        + new System.IO.FileInfo(clientFileName).Extension;
    if (string.IsNullOrEmpty(chunk))
    {
        // chunk为空，不分片
        file.SaveAs(System.IO.Path.Combine(_basePath, fileName));
        return Json(new { 
            Message = ConnString.Success, FileName = fileName 
        });
    }

    var index = Convert.ToInt32(chunk);//当前分块序号
    var guid = Request.Form["guid"];//前端传来的GUID号

    // 分片上传时，分片文件先临时存入chunkDir目录
    var filePath = System.IO.Path.Combine(_chunkDirPath, guid +"_"+index);
    if (file != null) //为null可能是暂停的那一瞬间
    {
        file.SaveAs(filePath);
    }
    return Json(new { 
        Message = ConnString.Success, Content = "chunk", FileName = fileName 
    });
}
```

<!-- \newpage -->

### plantUML-ER 图
```plantuml
@startuml
/'
  図の中で目立たせたいエンティティに着色するための
  色の名前（定数）を定義します。
'/
!define MAIN_ENTITY #E2EFDA-C6E0B4
!define MAIN_ENTITY_2 #FCE4D6-F8CBAD

/' 他の色も、用途が分りやすいように名前をつけます。 '/
!define METAL #F2F2F2-D9D9D9
!define MASTER_MARK_COLOR AAFFAA
!define TRANSACTION_MARK_COLOR FFAA00

/'
  デフォルトのスタイルを設定します。
  この場合の指定は class です。entity ではエラーになります。
'/
skinparam class {
    BackgroundColor METAL
    BorderColor Black
    ArrowColor Black
}

package "外部データベース" as ext <<Database>> {
    entity "顧客マスタ" as customer <<M,MASTER_MARK_COLOR>> {
        + 顧客ID [PK]
        --
        顧客名
        郵便番号
        住所
        電話番号
        FAX
    }
}

package "開発対象システム" as target_system {
    /'
      マスターテーブルを M、トランザクションを T などと安直にしていますが、
      チーム内でルールを決めればなんでも良いと思います。交差テーブルは "I" とか。
      角丸四角形が描けない代替です。
      １文字なら "主" とか "従" とか日本語でも OK だったのが受ける。
     '/
    entity "注文テーブル" as order <<主,TRANSACTION_MARK_COLOR>> MAIN_ENTITY {
        + 注文ID [PK]
        --
        # 顧客ID [FK]
        注文日時
        配送希望日
        配送方法
        お届け先名
        お届け先住所
        決済方法
        合計金額
        消費税額
    }

    entity "注文明細テーブル" as order_detail <<T,TRANSACTION_MARK_COLOR>> MAIN_ENTITY_2 {
        + 注文ID   [PK]
        + 明細番号 [PK]
        --
        # SKU [FK]
        注文数
        税抜価格
        税込価格
    }

    entity "SKUマスタ" as sku <<M,MASTER_MARK_COLOR>> {
        + SKU [PK]
        --
        # 商品ID [FK]
        カラー
        サイズ
        重量
        販売単価
        仕入単価
    }

    entity "商品マスタ" as product <<M,MASTER_MARK_COLOR>> {
        + 商品ID [PK]
        --
        商品名
        原産国
        # 仕入先ID [FK]
        商品カテゴリ
        配送必要日数
    }

    entity "仕入先マスタ" as vendor <<M,MASTER_MARK_COLOR>> {
        + 仕入先ID [PK]
        --
        仕入れ先名
        郵便番号
        住所
        電話番号
        FAX番号
    }
}

customer       |o-ri-o{     order
order          ||-ri-|{     order_detail
order_detail    }-do-||      sku
sku             }-le-||     product
product        }o-le-||     vendor

note bottom of customer : 別プロジェクト\nDB-Linkで参照する
@enduml
```
<!-- \newpage -->

### 基于角色的权限控制ER图
```plantuml
@startuml
' ER图定义
!define EntityColor1 #E2EFDA-C6E0B4
!define EntityColor2 #FCE4D6-F8CBAD
!define EntityColor3 #F2F2F2-D9D9D9
!define Table(name,desc) class name as "desc" << (T,#FFAAAA) >>
!define TableRelationships(name,desc) class name as "desc" << (E,#FFAA00) >> EntityColor1
!define TableEnum(name,desc) class name as "desc" << (E,#AAFFAA) >> EntityColor3
!define pk(x) + <b>x [PK]</b>
!define fk(x) # <i>x [FK]</i>
!define unique(x) <color:green>x</color>
!define not_null(x) <u>x</u>

hide methods
hide stereotypes

package "基于角色的权限控制ER图（RBAC）" as target_system{

' 实体（entities）
Table(user, "User （用户表）"){
    pk(UserId)  int  用户id
    --
    not_null(UserName)  nvarchar(50)    用户名
    not_null(UserStatus)    int  用户状态
    AddTime datetime  创建时间
}


Table(role, "Role （角色表）"){
    pk(RoleId)  int  角色id
    --
    not_null(RoleName)  nvarchar(50)    角色名称
    RoleDesc    nvarchar(50)    角色描述
}

TableRelationships(user_role, "User_Role （用户角色表）"){
    pk(UserId)  int  用户id
    pk(RoleId)  int  角色id
    --
}

Table(menu, "Menu （菜单）"){
    pk(MenuId)  int 菜单Id
    --
    not_null(MenuName)   nvarchar(10)    菜单名称
    ParentId    int  父Id
    Url nvarchar(1000)  对应链接
    Icon    nvarchar(20)  图标
    TargetType  int  打开方式（枚举）
    IsOpen  int  是否展开
}


TableRelationships(menu_role,"Menu_Role （菜单角色关系表）"){
    pk(MenuId)  int 菜单id
    pk(RoleId)  int 角色id
    --
    PValue  int 权限值
}

TableEnum(permission, "Permission （权限表）"){
    pk(PValue)  int 权限值
    --
    PName   nvarchar(10)    权限名称
}
note left of permission : 权限值定义为2进制的整形值，\n方便做权限的叠加判断。


' 关系（relationships）
' up:上方；do:下方；le:左侧；ri:右侧；
user    ||-do-|{    user_role
user_role   }|-do-||    role
role    ||-ri-|{    menu_role :\t\t\t
menu_role   ||-up-|{    menu

' 布局（layout）
menu     -[hidden]->    permission
role     -[hidden]->    permission

}
@enduml
```

<!-- \newpage -->

### 时序图
```plantuml
@startuml
' 这里写注释
skinparam backgroundColor #EEEBDC
skinparam handwritten true
' 样式定义结束

actor User
participant "First Class" as A
participant "Second Class" as B
participant "Last Class" as C

User -> A: DoWork
activate A

A -> B: Create Request
activate B

B -> C: DoWork
activate C
C --> B: WorkDone
destroy C

B --> A: Request Created
deactivate B

A --> User: Done
deactivate A
@enduml
```

<!-- \newpage -->

### 域名解析流程图
```plantuml
@startuml
title 域名解析流程图
skinparam state {
  StartColor MediumBlue
  EndColor Red
  BorderColor Gray
  ArrowColor Gray
  ArrowFontColor Indigo
  BackgroundColor<<Request>> AliceBlue
  BackgroundColor<<Answer>> HotPink
}

state UserClient {
    [*] --> 请求访问域名
    请求访问域名 --> 本机host
    本机host -right-> 成功访问域名 : 有映射
    本机host --> 本机DNS解析器 : 没有映射
    本机DNS解析器 -up-> 成功访问域名 : 有缓存
    成功访问域名 --> 本机DNS解析器 : 缓存更新
    本机DNS解析器 --> [*]
    note left : 本图展示正常解析流程\n未成功访问域名不能结束\n（最左支路径无效）
    
    state 请求访问域名 : ephen.me.
    state 请求访问域名<<Request>>
    state 本机host : Windows 目录：
    state 本机host : C:\Windows\System32\drivers\etc
    state 成功访问域名 : ephen.me.
    state 成功访问域名<<Answer>>
    state 本机DNS解析器 : Windows 查看命令：
    state 本机DNS解析器 : ipconfig /displaydns
}

state DNSServer {
    本机DNS解析器 -right-> 本地DNS : 没有缓存
    本地DNS -right-> 根域名服务器 : 没有解析
    本地DNS -left-> 成功访问域名 : 有解析
    根域名服务器 --> 顶级域名服务器
    顶级域名服务器 --> 域名服务器
    域名服务器 --> 本地DNS : 获取结果并缓存

    state 本地DNS : 用户配置（8.8.8.8）
    state 根域名服务器 : a.root-servers.net.
    state 根域名服务器 : b.root-servers.net.
    state 根域名服务器 : ...
    state 根域名服务器 : m.root-servers.net.
    state 顶级域名服务器 : a0.nic.me.
    state 顶级域名服务器 : b0.nic.me.
    state 顶级域名服务器 : ...
    state 域名服务器 : lv3ns1.ffdns.net.
    state 域名服务器 : lv3ns2.ffdns.net.
    state 域名服务器 : lv3ns3.ffdns.net.
    state 域名服务器 : lv3ns4.ffdns.net.
}
@enduml
```