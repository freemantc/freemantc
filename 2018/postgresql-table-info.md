# PostgreSQL 获取表结构

<i class="fa fa-code"></i>

```sql
-- PostgreSQL 获取所有表名
SELECT relname as tab_name,
coalesce（obj_description(relfilenode,'pg_class'),'') as tab_comment
FROM pg_class c WHERE relname IN (SELECT tablename FROM pg_tables WHERE schemaname ='public')

-- 获取表的所有信息
SELECT DISTINCT
    a.attnum as index_num,
    a.attname as field_name,
    format_type(a.atttypid, a.atttypmod) as field_type,
    a.attnotnull as is_not_null, 
    com.description as field_comment,
    coalesce(i.indisprimary,false) as primary_key,
    def.adsrc as field_default
FROM pg_attribute a 
JOIN pg_class pgc ON pgc.oid = a.attrelid
LEFT JOIN pg_index i ON 
    (pgc.oid = i.indrelid AND i.indkey[0] = a.attnum)
LEFT JOIN pg_description com on 
    (pgc.oid = com.objoid AND a.attnum = com.objsubid)
LEFT JOIN pg_attrdef def ON 
    (a.attrelid = def.adrelid AND a.attnum = def.adnum)
WHERE a.attnum > 0 AND pgc.oid = a.attrelid
AND pg_table_is_visible(pgc.oid)
AND NOT a.attisdropped
AND pgc.relname = 'table_1'  -- Your table name here
ORDER BY a.attnum;
```