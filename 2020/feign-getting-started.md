# Feign 入门指引

## 什么是 Feign
Feign是一种声明式Http客户端，灵感来源于`Retrofit`、`JAXRS-2.0` 和 `WebSocket`，它旨在通过最少的资源和代码来实现和HTTP Client的能力。并且通过可定制的解码器和错误处理，编写任意的HTTP API。Feign使得 Java HTTP 客户端编写更方便。

> 该项目的github地址是：https://github.com/OpenFeign/feign

## Netflix Feign / Open Feign
Feign属于Netflix开源的组件

1. GAV坐标差异

```xml
<dependency>
    <groupId>com.netflix.feign</groupId>
    <artifactId>feign-core</artifactId>
</dependency>

<dependency>
    <groupId>io.github.openfeign</groupId>
    <artifactId>feign-core</artifactId>
</dependency>
```

2. 官网地址差异

- Netflix Feign：https://github.com/Netflix/feign
- Open Feign：https://github.com/OpenFeign/feign

*不过现在访问 https://github.com/Netflix/feign 已经被重定向到了后者上。*

3. 发版历史

- Netflix Feign：1.0.0发布于2013.6，于2016.7月发布其最后一个版本8.18.0
- Open Feign：首个版本便是9.0.0版，于2016.7月发布，然后一直持续发布到现在（未停止）

4. 总结

> 简单说，Netflix Feign是停止维护的老版本，Open Feign是持续维护的新版本。
> 在没有老项目的情况下，直接用新版本即可。

## 如何使用

### Spring Boot 程序接入（通用java程序）

#### pom

```xml
<properties>
    <open-feign.version>11.0</open-feign.version>
</properties>

<!-- open feign -->
<dependency>
    <groupId>io.github.openfeign</groupId>
    <artifactId>feign-core</artifactId>
    <version>${open-feign.version}</version>
</dependency>
```

#### 基础用法
1. 定义接口
```java
public interface IPublicApiV2Client {
    @RequestLine("GET /areas")
    String areas();

    @RequestLine("GET /banks/{bankId}/grades")
    String grades(@Param("bankId") Integer bankId);
}
```

2. 创建客户端
```java
@Bean
public IPublicApiV2Client publicApiV2Client() {
    return Feign.builder().target(
        IPublicApiV2Client.class,       // 类型
        "http://api.xkw.com/zujuan/v2"  // 地址
    );
}
```

3. 调用
```java
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class ApplicationTests {

    @Autowired 
    private IPublicApiV2Client publicApiV2Client;

    @Test
    public void test_api() {
        try {
            var areasResult = publicApiV2Client.areas();
            log.info(areasResult);
        } catch (FeignException fe) {
            if(fe.status()>-1) {
                log.error("areas 返回异常【{}】：{}", fe.status(), fe.contentUTF8());
            } else {
                log.error("网络请求异常：{}", fe.getMessage());
            }
        }

        try {
            var gradesResult = publicApiV2Client.grades(11);
            log.info(gradesResult);
        } catch (FeignException fe) {
            if(fe.status()>-1) {
                log.error("grades 返回异常【{}】：{}", fe.status(), fe.contentUTF8());
            } else {
                log.error("网络请求异常：{}", fe.getMessage());
            }
        }
    }
}
```

#### 自定义Header
1. 实现 `RequestInterceptor` 接口
```java
public class JsonRequestInterceptor implements RequestInterceptor {

    private static final String ContentTypeName = "Content-Type";
    private static final String ContentTypeValue = "application/json;charset=UTF-8";

    @Override
    public void apply(RequestTemplate requestTemplate) {
        requestTemplate.header(ContentTypeName, ContentTypeValue);
    }
}
```

2. 创建客户端的时候引入
```java
@Bean
public IPublicApiV2Client publicApiV2Client() {
    return Feign.builder()
        .requestInterceptor(new JsonRequestInterceptor())
        .target(IPublicApiV2Client.class,"http://api.xkw.com/zujuan/v2");
}
```

#### 自定义编码器和解码器
1. 实现 `Encoder` 接口
```java
public class FastJsonFeignEncoder implements Encoder {
    @Override
    public void encode(Object o, Type type, RequestTemplate requestTemplate) throws EncodeException {
        requestTemplate.body(JSON.toJSONBytes(o,
                SerializerFeature.WriteNullNumberAsZero,
                SerializerFeature.WriteNullListAsEmpty
        ), Util.UTF_8);
    }
}
```

2. 实现 `Decoder` 接口
```java
public class FastJsonFeignDecoder implements Decoder {
    @Override
    public Object decode(Response response, Type type) throws IOException, FeignException {
        var body = response.body();
        if ( body == null) {
            return null;
        }
        if(String.class.equals(type)) {
            return Util.toString(body.asReader(Util.UTF_8));
        }
        if(byte[].class.equals(type)){
            return Util.toByteArray(body.asInputStream());
        }
        return JSON.parseObject(body.asInputStream(), Util.UTF_8, type, null);
    }
}
```

3. 创建客户端的时候引入
```java
@Bean
public IPublicApiV2Client publicApiV2Client() {
    return Feign.builder()
        .requestInterceptor(new JsonRequestInterceptor())
        .encoder(new FastJsonFeignEncoder())
        .decoder(new FastJsonFeignDecoder())
        .target(IPublicApiV2Client.class,"http://api.xkw.com/zujuan/v2");
}
```

#### 更换Http请求客户端
1. pom引入
```xml
<dependency>
    <groupId>io.github.openfeign</groupId>
    <artifactId>feign-okhttp</artifactId>
    <version>${open-feign.version}</version>
</dependency>
```

2. 创建客户端的时候引入
```java
@Bean
public IPublicApiV2Client publicApiV2Client() {
    return Feign.builder()
        .requestInterceptor(new JsonRequestInterceptor())
        .encoder(new FastJsonFeignEncoder())
        .decoder(new FastJsonFeignDecoder())
        .client(new OkHttpClient())
        .target(IPublicApiV2Client.class,"http://api.xkw.com/zujuan/v2");
}
```

#### 处理日志
1. pom引入
```xml
<dependency>
    <groupId>io.github.openfeign</groupId>
    <artifactId>feign-slf4j</artifactId>
    <version>${open-feign.version}</version>
</dependency>
```

2. 创建客户端的时候引入
```java
@Bean
public IPublicApiV2Client publicApiV2Client() {
    return Feign.builder()
        .requestInterceptor(new JsonRequestInterceptor())
        .encoder(new FastJsonFeignEncoder())
        .decoder(new FastJsonFeignDecoder())
        .client(new OkHttpClient())
        .logger(new Slf4jLogger())
        .logLevel(Logger.Level.FULL)
        .target(IPublicApiV2Client.class,"http://api.xkw.com/zujuan/v2");
}
```

3. application.yml
```yaml
logging:
  level:
    feign.Logger: debug
```

#### 创建客户端其他注意事项
```java
@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public IPublicApiV2Client publicApiV2Client() {
        return Feign.builder()
            .requestInterceptor(new JsonRequestInterceptor())
            .requestInterceptor(new BasicAuthRequestInterceptor("username", "password")) // BasicAuth
            .encoder(new FastJsonFeignEncoder())
            .decoder(new FastJsonFeignDecoder())
            .client(new OkHttpClient())
            .logger(new Slf4jLogger())
            .logLevel(Logger.Level.FULL)
            .retryer(Retryer.NEVER_RETRY) // 默认情况下，Feign是有重试机制的，并且是100ms重试一次，默认重试5次。生产环境下非特殊情况，建议关闭。
            .options(new Request.Options(500L, TimeUnit.MILLISECONDS, 60L, TimeUnit.SECONDS, true)) // 各种超时设置
            .target(IPublicApiV2Client.class,"http://api.xkw.com/zujuan/v2");
    }

    // 另一种写法
    @Bean
    public IPublicApiV2Client publicApiV2Client() {
        return Feign.builder()
            .requestInterceptors(Arrays.asList(
                    new JsonRequestInterceptor(),
                    new BasicAuthRequestInterceptor("username", "password")
            )) // 如果一次性添加多个，那么此方法相当于set方法，并不是add
            .encoder(new FastJsonFeignEncoder())
            .decoder(new FastJsonFeignDecoder())
            .client(new OkHttpClient())
            .logger(new Slf4jLogger())
            .logLevel(Logger.Level.FULL)
            .retryer(Retryer.NEVER_RETRY) // 默认情况下，Feign是有重试机制的，并且是100ms重试一次，默认重试5次。生产环境下非特殊情况，建议关闭。
            .options(new Request.Options(500L, TimeUnit.MILLISECONDS, 60L, TimeUnit.SECONDS, true)) // 各种超时设置
            .target(IPublicApiV2Client.class,"http://api.xkw.com/zujuan/v2");
    }

}
```

#### 更多示例
```java
public interface IPublicApiV2Client {

    // 普通无参数GET情求
    @RequestLine("GET /areas")
    String areas();

    // @Param注解，路径参数GET情求
    @RequestLine("GET /banks/{bankId}/grades")
    String grades(@Param("bankId") Integer bankId);

    // @Param注解，QueryString参数GET情求
    @RequestLine("GET /schools/top100?pageIndex={pageIndex}&pageSize={pageSize}")
    String topSchool(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize);

    // @QueryMap注解，弱类型GET情求
    @RequestLine("GET /questions/{bankId}")
    String quesList(@Param("bankId") Integer bankId, @QueryMap Map<String, Object> queryMap);

    // @QueryMap注解，强类型GET情求
    @RequestLine("GET /papers/{bankId}")
    String paperList(@Param("bankId") Integer bankId, @QueryMap PaperListQueryMap queryMap);

    // body raw方式，弱类型POST请求
    @Body("{body}")
    @RequestLine("POST /internal/log/list")
    String logList(@Param("body") String body);

    // body raw方式，强类型POST请求
    @RequestLine("POST /internal/log/list")
    PagerResultContent<List<LogES>> remoteLogList(LogListReq body);
}
```

### Spring Cloud 程序接入
> **敬请期待**